from django.shortcuts import render, redirect, reverse
from django.http import JsonResponse, HttpResponseRedirect
import requests
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse


# Create your views here.
def user_login(request):
    context ={}
    if request.method == "POST" :
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return HttpResponseRedirect(reverse('user_success'))

        else:
            context["error"]= "Nama pengguna atau kata sandi anda salah"
            return render(request, 'login.html', context)

    else:
        return render(request, 'login.html', context)

def user_success(request): 
    context = {}
    context['user'] = request.user
    return render(request, 'success.html', context)

def user_logout(request):
    if request.method == "POST":
        logout(request)
        return HttpResponseRedirect(reverse('user_login'))
