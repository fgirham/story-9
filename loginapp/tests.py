from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import user_login,user_logout,user_success
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.auth.models import User

# Create your tests here.

class LoginAppUnitTest(TestCase):

    def test_ada_halaman_login(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    
    def test_ada_halaman_success(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_bisa_login(self):
        user = User.objects.create_user('irham', 'fghaniirham1@gmail.com', 'cobain123')
        response = Client().post('/login/', {'username': 'irham', 'password': 'cobain123'})
        self.assertEqual(response.status_code, 302)
    
    def test_gagal_login(self):
        user = User.objects.create_user('irham', 'fghaniirham1@gmail.com', 'cobain123')
        response = Client().post('/login/', {'username': 'irham', 'password': 'cobain321'})
        self.assertEqual(response.status_code, 200)

    def test_bisa_logout(self):
        response = Client().post('/logout/')
        self.assertEqual(response.status_code, 302)