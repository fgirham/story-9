from django.contrib import admin 
from django.urls import path
from . import views

urlpatterns =[
    path('', views.user_login,name="user_login"),
    path('login/', views.user_login,name="user_login"),
    path('home/', views.user_success,name="user_success"), 
    path('logout/', views.user_logout,name="user_logout"),
]
